/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  *
 ******************************************************************************
 *  $File Name$       : kf_ic.c                                               *
 *  $Author$          : ChipON AE/FAE Group                                   *
 *  $Data$            : 2021-07-8                                             *
 *  $Hard Version     : KF32A156-MINI-EVB_V1.2                                *
 *  $Description$     : Main Interrupt Service Routines.                      *      
 *                      This file provides template for all exceptions        *
 *                      handler and peripherals interrupt service routine.    *
 ******************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 *
 *  All rights reserved.                                                      *
 *                                                                            *
 *  This software is copyright protected and proprietary to                    *
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 *
 ******************************************************************************
 *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  *
 *                     		REVISON HISTORY                               	  *
 *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  *
 *  Data       Version  Author        Description                             *
 *  ~~~~~~~~~~ ~~~~~~~~ ~~~~~~~~~~~~  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  *
 *  2021-07-08 00.02.00 FAE Group     Version 2.0 update                      *
 *                                                                            *
 *                                                                            *
 *****************************************************************************/

/******************************************************************************
**                          Include Files                                    **
******************************************************************************/
#include "system_init.h"
#include "can_driver.h"
#include "can_buffer.h"
#include "can_uds.h"
/*******************************************************************************
**                   KF32A156 Processor Exceptions Handlers  		         **
*******************************************************************************/

//*****************************************************************************************
//                                 NMI Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _NMI_exception (void)
{	

}

//*****************************************************************************************
//                               HardFault Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _HardFault_exception (void)
{

}

//*****************************************************************************************
//                               StackFault Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _StackFault_exception (void)
{

}

//*****************************************************************************************
//                               SVC Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _SVC_exception (void)
{

}

//*****************************************************************************************
//                              SoftSV Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _SoftSV_exception (void)
{

}

//*****************************************************************************************
//                              SysTick Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _SysTick_exception (void)
{
	
}

//*****************************************************************************************
//                             	 CAN4 Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _CAN4_exception (void)
{
	volatile Can_Pdu_TypeDef Can_Pdu_Receive;
	volatile uint32_t Can_Rcr = 0x00;

	if(CAN_Get_INT_Flag(CAN4_SFR,CAN_INT_BUS_ERROR))
	{
		CAN_Clear_INT_Flag(CAN4_SFR,CAN_INT_BUS_ERROR);
		Can_Rcr = CAN4_SFR->RCR;
	}

	if(CAN_Get_INT_Flag(CAN4_SFR,CAN_INT_ERROR_NEGATIVE))
	{
		CAN_Clear_INT_Flag(CAN4_SFR,CAN_INT_ERROR_NEGATIVE);
	}

	if(CAN_Get_INT_Flag(CAN4_SFR,CAN_INT_TRANSMIT))
	{
		CAN_Clear_INT_Flag(CAN4_SFR,CAN_INT_TRANSMIT);
	}

	if(CAN_Get_INT_Flag(CAN4_SFR,CAN_INT_BUS_OFF))
	{
		CAN_Clear_INT_Flag(CAN4_SFR,CAN_INT_BUS_OFF);
		CAN4_SFR->CTLR &= ~(0x01);
	}

	if(CAN_Get_INT_Flag(CAN4_SFR,CAN_INT_RECEIVE))
	{
		CAN_Receive_Message(&Can_Pdu_Receive);
		for(int i=0;i<Can_Pdu_Receive.Frame_length;i++){
			CAN_MSG *pRxMsg = CAN_BUF_PickMsg(0);
			//对接收到的数据先做简单的判断，去掉错误的数据
#if MSG_ID_TYPE
			if((Can_Pdu_Receive.CAN_Message[i].m_FrameFormat==CAN_FRAME_FORMAT_EFF)&&((Can_Pdu_Receive.CAN_Message[i].m_Can_ID == PHY_ADDR_ID)||(Can_Pdu_Receive.CAN_Message[i].m_Can_ID == FUN_ADDR_ID))){
#else
			if((Can_Pdu_Receive.CAN_Message[i].m_FrameFormat==CAN_FRAME_FORMAT_SFF)&&((Can_Pdu_Receive.CAN_Message[i].m_Can_ID == PHY_ADDR_ID)||(Can_Pdu_Receive.CAN_Message[i].m_Can_ID == FUN_ADDR_ID))){
#endif
				pRxMsg->Flags.IDE = Can_Pdu_Receive.CAN_Message[i].m_FrameFormat;
				pRxMsg->Flags.RTR = 0;
				pRxMsg->Flags.DIR = 0;
				pRxMsg->Flags.DLC = Can_Pdu_Receive.CAN_Message[i].m_DataLength;
				memcpy(pRxMsg->Data,(const void *)Can_Pdu_Receive.CAN_Message[i].m_Data,8);
				CAN_BUF_ApendMsg(0);
			}
		}
	}

	if(CAN_Get_INT_Flag(CAN4_SFR,CAN_INT_DATA_OVERFLOW))
	{
		CAN_Clear_INT_Flag(CAN4_SFR,CAN_INT_DATA_OVERFLOW);
	}
}
